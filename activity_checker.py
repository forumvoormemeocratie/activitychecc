import requests
import json
import time


CLAN = 'redditors' #YOUR CLAN NAME HERE
exceptions = ['example1', 'example2'] #Put members who you don't want to show here in lowercase


PLAYER_URL = 'http://services.runescape.com/m=website-data/playerDetails.ws?membership=true&names='
RUNEMETRICS_URL = 'https://apps.runescape.com/runemetrics/profile/profile?user='
CLAN_URL = 'http://services.runescape.com/m=clan-hiscores/members_lite.ws?clanName='
REFERER_URL = 'http://services.runescape.com/'
CALLBACK = 'callback=jQuery000000000000000_0000000000'
n = 10

member_list = []

def _get_members():
    response = requests.get(str(CLAN_URL+CLAN))
    data = response.text.split('\n')[1:-1]

    members = [i.split(',') for i in data]

    for member in members:
        member[0] = member[0].replace('\xa0', ' ')
        member_list.append(member)

    members = [member[0] for member in members]

    return members


def _chunks(members, n):
    chunk_size = int(len(members)/n)
    chunks = [members[x:x+chunk_size] for x in range(0, len(members), chunk_size)]
    for chunk in chunks:
            yield '["{}"]'.format('","'.join([x for x in chunk]))

def parse_response(response):
    response = response.decode('utf-8')
    response = response[response.index('(') + 1:response.index(')')]
    return json.loads(response)


def _fetch(chunk):
    target = f'{PLAYER_URL}{chunk}&{CALLBACK}'
    headers = {'Referer':REFERER_URL}
    data = requests.get(target, headers=headers).text
    response = data[data.index('(') + 1:data.index(')')]

    return json.loads(response)


def process(members):
    memberlist = []
    for member in members:
        memberlist += member

    nonmembers = []
    for member in memberlist:
        if not member['member'] and member['name'].lower() not in exceptions:
            nonmembers.append(member['name'])

    return nonmembers


def check_person(name):
    player = {'name': name}

    target = f'{RUNEMETRICS_URL}{name}'
    headers = {'Referer':REFERER_URL}
    data = requests.get(target, headers=headers).text
    response = json.loads(data)

    if 'error' in response:
        player['last'] = 'Error'
        return player

    if 'activities' in response:
        if len(response['activities']) == 0:
            player['last'] = 'N/A'
        else:
            player['last'] = response['activities'][1]['date']

    return player


def build_persons(persons):
    #as far from efficient as you could go, but using xp and rank were an afterthought
    #and I didnt want to rewrite all the code

    #rip 2 seconds of your life whenever you run this script instead of a more efficient one
    built = []

    for person in persons:
        for member in member_list:
            if person['name'] == member[0]:
                person['rank'] = member[1]
                person['xp'] = int(member[2])

        built.append(person)
    return built

def print_person(person):
    body = person['name'] + (' ' * (20-len(person['name']))) + '\t'
    body += person['rank'] + (' ' * (15-len(person['rank']))) + '\t'
    body += '{:,}'.format(person['xp']) + (' ' * (15-len(str(person['xp'])))) + '\t'
    body += person['last']

    print(body)

def run(n):
    print(f'Getting clan members of {CLAN}')
    members = [_fetch(chunk) for chunk in _chunks(_get_members(), n)]
    nonmembers = process(members)

    persons = []
    count = 0
    total = len(nonmembers)

    print(f'Checking last activity for non-members in {CLAN}')
    for member in nonmembers:
        count += 1
        print(f' >> checking {member} ({count}/{total})')
        persons.append(check_person(member))
        time.sleep(1)

    persons = build_persons(persons)
    sorted_persons = sorted(persons, key=lambda k: k['xp'])

    for person in sorted_persons:
        print_person(person)


if __name__ == '__main__':
    run(n)
    input('')
